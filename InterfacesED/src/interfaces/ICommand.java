package interfaces;
/*
 * Interface IComand
 */
public interface ICommand {
	/*
	 * Este metodo deve crear un IComand y devolver <strong>true</strong> si se a podido
	 * crear.
	 */
	public Boolean create(String string);
	/*
	 * Este metodo deve implementarse de manera que ponga en marxa un IComand.
	 */
	public Boolean run();
}
