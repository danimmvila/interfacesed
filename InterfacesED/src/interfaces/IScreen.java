package interfaces;
/*
 * Interfaz que deve implementar un programa para mostrar en pantalla...
 */
public interface IScreen {
	/*
	 * Este metodo devuelve ancho de pantalla.
	 */
	public int getWidth();
	
	/*
	 * metodo para canviar ancho de pantalla.
	 */
	public void setWidth(int width);
	
	/*
	 * Este metodo devuelve altura de pantalla.
	 */
	public int getHeight();
	
	/*
	 * Metodo para cambiar altura de pantalla.
	 */
	public void setHeight(int height);
	
	/*
	 * Metodo para crear...
	 */
	public Boolean create();
	
	/*
	 * El meetodo Start inicia la ejecucion de un IScreen.
	 */
	public void start();
	
	/*
	 * El metodo Stop recive como parametro un booleno (Si es <strong>true</strong> detiene 
	 * la ejecucion del objeto al que se le aplica.
	 */
	public void stop(Boolean force);
	
	/*
	 * El metodo close cierra el objeto que impllementa esta interface.
	 */
	public void close(Boolean force);
	
	/*
	 * El metodo readComoand devuelve un IComand de un objeto que implementa la
	 * interface IScreen
	 */
	public ICommand readCommand();
	
	/*
	 * El metodo executeComand recive como parametro un IComand, trata de ejecutarlo
	 * y devuelve un booleano (<strong>true</strong> si se ha podido ejecutar y <strong>false</strong>
	 * si no se a podido.
	 */
	public Boolean executeCommand(ICommand command);
}
