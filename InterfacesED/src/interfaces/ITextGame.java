package interfaces;
/*
 * Esta interface no la entiendo...
 */
public interface ITextGame {
	/*
	 * Este metodo deve inicializaz un ITextGame.
	 */
	public void start();
	
	/*
	 * Este metodo deve detener un ITextGame.
	 */
	public void stop(Boolean force);
	
	/*
	 * Este metodo deve pausar un ITextGame.
	 */
	public void pause(Boolean force);
	
	/*
	 * Este metodo deve devolver un IScreen de un ITextGame.
	 */
	public IScreen getScreen();
}
